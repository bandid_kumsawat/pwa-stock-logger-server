var router = require('express').Router();
const { GoogleSpreadsheet } = require('google-spreadsheet');
const { isEqual } = require('underscore');
const provicePWA = require("./privice.json")

const sheet_id = '1ufYy1EIevysE_olTALCzTYkSkmVBNdlICUbFFn_52M4'

router.get('/stock/provice', async function(req, res, next){
  res.json(provicePWA);
});

router.get('/stock/history', async function(req, res, next){
  try{
    const doc = new GoogleSpreadsheet(sheet_id);
    await doc.useServiceAccountAuth({
      client_email: 'sheets@api-nodejs-sheet.iam.gserviceaccount.com',
      private_key: '-----BEGIN PRIVATE KEY-----\nMIIEuwIBADANBgkqhkiG9w0BAQEFAASCBKUwggShAgEAAoIBAQCpiQgMnBw6Rha+\n0mrYl7WMXTcszzhAo15FuynnVtZm+58Exlxs+UoixvC4FMf4XjnfMQ/K1k9wGVYd\nBjtceTao7kf3MQQYZgQMesOIcL9g0lCd+NQhFR7cQ8yYo0GsiH4+NOly9uqVUBZS\nZaRaiB0zqnxX9PUd/g1K5L9NMdPfOsbRL/hTtvctcTUWzpUe0xKV3YKlo88DKK7n\n7vi3V1vxrulWZC2Q5PsT2fz2F9c/JM1Zv4yii8LyIMVD/scYxqcQSJCMhH46RJR4\nw2AdAvHR8bB7ZolyI9anLoUYpkx8SshfpMbG8IGq28vyKJGTxk3V38c3SFgT9EsH\nTj+aiaHNAgMBAAECgf9RaGliyUp7jHWEK/PZVrbpC0eFmETGMtD/qvJh7PV068w5\nR+JjA+QVhd+jqcSlbJmmpUPdxy+nJINsI1sUsR/pkMsiZ8J9Yn9GERrTe+KzG28s\n/mFwpeqHI3UI+xLE/Vgzz+tuNrUlIj2VwRRhiR9YC06qElGhlUpfB50d9E9IzvEx\nXPx/M2KBt1H2/RyGMkIxwvaraCZXEIMp+4Hd9NcSCHF6QN/DrsBn8wbODqlHBrwW\nXzjP8xQe1kY9kffDZfct3K1JpDF8BSfYdYFb2PoQYpDaZooc7nw74gTjyPuEY76e\nFiNioRfD8GuqWRpFq59pmzX1t4z2bID90J1dLvMCgYEA3dCzCpMhrGA+pidEFtAz\noKKzLqvqDZhLONPvZR4MhSAk5MMx7Oca+BgsNiV0domP8CwyCOz66UDq8d3uhgPp\nrUFhEJUY38MJsGtGATC/xwrp/D/fjtlYw9qr+7BuQeI2cWzn2vwAvDsdr1dS8DbG\n03eWWmeoITGe62GN1gvBkR8CgYEAw6m5sTReNA6WIrb3qpuhPHp8jkfKyriDU/wP\nbwWokSWy5VC9ytDK3vMUQeW8JBDSRc94C5TH93IEqNfiTXokyspayLD+U6342et8\nC45CxAavhZ+03nRo14yGLMM3RCJNzq15c10MOmzZinZtGGk40Tb8abLFFTChKFb+\nXnmiE5MCgYEAt5ANitPFyxv+3IukKmj0QJxEQnxcfhGpNocVFw4t+EFoprPubgm2\n5NdERy7p2Wrv5ib85SwQXNWvWKd7zAYS7KEtTYbFhe+m5gtwbG3GV1bt2iWVwWaW\nscYWC3yvUfB9c5JXsX5YsW0OnNtcNJRwJ/JQR9J+bbHdvRcHRLqH3qsCgYAGdrPo\ns7cs9xJkoTC7VRrVeaJlvN9pJQFJo/kKjOBNszYetzUd3xnklZkkuCcn9dpMbhck\nIs0cUoru3Prf3loazOaE3Lv6HbQr1sipRRDVkvb3bhg2hRf1jvHbxrMrsApr+nUZ\np4hA4dmp5lGiTc+PscZS45H5KjsYrw8GJfSBuwKBgCtHUM4fKHVhF35RLJ1D78Q2\nbufhZFOY6knsRe2gRVf041Bg8LyO0o3lbG5oowTgLZkD+WWbRPbC7NfJFmxpzgHB\ni+0Pke+dBtCH12zPRnBy6Nrjy6KQVwfAX/KC3qeXvDXJ8Y13m/Ro2yTljqWwRz1O\n4jBsCej2adMQmzWKv8ks\n-----END PRIVATE KEY-----\n',
    });
    await doc.useServiceAccountAuth(require('./client_secret.json'), 'sheets@api-nodejs-sheet.iam.gserviceaccount.com');
    await doc.loadInfo(); 
    // stock
    const sheet_stock = doc.sheetsByIndex[1]; // the first sheet
    const rows_stock = await sheet_stock.getRows();
  
    // log
    const sheet_log = doc.sheetsByIndex[2]; // the first sheet
    const rows_log = await sheet_log.getRows();
  
    var temp = []
    for (var i = 0;i < rows_stock.length;i++){
      var log = []
      for (var m = 0;m < rows_log.length;m++){
        if (rows_log[m]['serial'] === rows_stock[i]['serial']){
          log.push({
            "serial": rows_log[m].serial,
            "status": rows_log[m].status,
            "area": rows_log[m].area,
            "branch": rows_log[m].branch,
            "datetime": rows_log[m].datetime,
          })
        }
      }
      temp.push({
        "serial": rows_stock[i]['serial'],
        "area": rows_stock[i]['area'],
        "branch": (rows_stock[i]['branch'] === undefined ? "" : rows_stock[i]['branch']),
        "status": rows_stock[i]['status'],
        "time_instock": (rows_stock[i]['time_instock'] === undefined ? "" : rows_stock[i]['time_instock']),
        "time_takeout": (rows_stock[i]['time_takeout'] === undefined ? "" : rows_stock[i]['time_takeout']),
        "time_installed": (rows_stock[i]['time_installed'] === undefined ? "" : rows_stock[i]['time_installed']),
        "time_maintenance": (rows_stock[i]['time_maintenance'] === undefined ? "" : rows_stock[i]['time_maintenance']),
        'log': log,
        'parcel_no': rows_stock[i]['parcel_no']
      })
    }
    res.json(temp);
  }catch(e){
    res.json({error: e,status: false});
  }
});

router.get('/find/all', async function(req, res, next){
  const doc = new GoogleSpreadsheet(sheet_id);
  await doc.useServiceAccountAuth({
    client_email: 'sheets@api-nodejs-sheet.iam.gserviceaccount.com',
    private_key: '-----BEGIN PRIVATE KEY-----\nMIIEuwIBADANBgkqhkiG9w0BAQEFAASCBKUwggShAgEAAoIBAQCpiQgMnBw6Rha+\n0mrYl7WMXTcszzhAo15FuynnVtZm+58Exlxs+UoixvC4FMf4XjnfMQ/K1k9wGVYd\nBjtceTao7kf3MQQYZgQMesOIcL9g0lCd+NQhFR7cQ8yYo0GsiH4+NOly9uqVUBZS\nZaRaiB0zqnxX9PUd/g1K5L9NMdPfOsbRL/hTtvctcTUWzpUe0xKV3YKlo88DKK7n\n7vi3V1vxrulWZC2Q5PsT2fz2F9c/JM1Zv4yii8LyIMVD/scYxqcQSJCMhH46RJR4\nw2AdAvHR8bB7ZolyI9anLoUYpkx8SshfpMbG8IGq28vyKJGTxk3V38c3SFgT9EsH\nTj+aiaHNAgMBAAECgf9RaGliyUp7jHWEK/PZVrbpC0eFmETGMtD/qvJh7PV068w5\nR+JjA+QVhd+jqcSlbJmmpUPdxy+nJINsI1sUsR/pkMsiZ8J9Yn9GERrTe+KzG28s\n/mFwpeqHI3UI+xLE/Vgzz+tuNrUlIj2VwRRhiR9YC06qElGhlUpfB50d9E9IzvEx\nXPx/M2KBt1H2/RyGMkIxwvaraCZXEIMp+4Hd9NcSCHF6QN/DrsBn8wbODqlHBrwW\nXzjP8xQe1kY9kffDZfct3K1JpDF8BSfYdYFb2PoQYpDaZooc7nw74gTjyPuEY76e\nFiNioRfD8GuqWRpFq59pmzX1t4z2bID90J1dLvMCgYEA3dCzCpMhrGA+pidEFtAz\noKKzLqvqDZhLONPvZR4MhSAk5MMx7Oca+BgsNiV0domP8CwyCOz66UDq8d3uhgPp\nrUFhEJUY38MJsGtGATC/xwrp/D/fjtlYw9qr+7BuQeI2cWzn2vwAvDsdr1dS8DbG\n03eWWmeoITGe62GN1gvBkR8CgYEAw6m5sTReNA6WIrb3qpuhPHp8jkfKyriDU/wP\nbwWokSWy5VC9ytDK3vMUQeW8JBDSRc94C5TH93IEqNfiTXokyspayLD+U6342et8\nC45CxAavhZ+03nRo14yGLMM3RCJNzq15c10MOmzZinZtGGk40Tb8abLFFTChKFb+\nXnmiE5MCgYEAt5ANitPFyxv+3IukKmj0QJxEQnxcfhGpNocVFw4t+EFoprPubgm2\n5NdERy7p2Wrv5ib85SwQXNWvWKd7zAYS7KEtTYbFhe+m5gtwbG3GV1bt2iWVwWaW\nscYWC3yvUfB9c5JXsX5YsW0OnNtcNJRwJ/JQR9J+bbHdvRcHRLqH3qsCgYAGdrPo\ns7cs9xJkoTC7VRrVeaJlvN9pJQFJo/kKjOBNszYetzUd3xnklZkkuCcn9dpMbhck\nIs0cUoru3Prf3loazOaE3Lv6HbQr1sipRRDVkvb3bhg2hRf1jvHbxrMrsApr+nUZ\np4hA4dmp5lGiTc+PscZS45H5KjsYrw8GJfSBuwKBgCtHUM4fKHVhF35RLJ1D78Q2\nbufhZFOY6knsRe2gRVf041Bg8LyO0o3lbG5oowTgLZkD+WWbRPbC7NfJFmxpzgHB\ni+0Pke+dBtCH12zPRnBy6Nrjy6KQVwfAX/KC3qeXvDXJ8Y13m/Ro2yTljqWwRz1O\n4jBsCej2adMQmzWKv8ks\n-----END PRIVATE KEY-----\n',
  });
  await doc.useServiceAccountAuth(require('./client_secret.json'), 'sheets@api-nodejs-sheet.iam.gserviceaccount.com');
  await doc.loadInfo(); 
  const sheet = doc.sheetsByIndex[1]; // the first sheet
  const rows = await sheet.getRows();
  var temp = []
  for (var i = 0;i < rows.length;i++){
    temp.push({
      "serial": rows[i]['serial'],
      "area": rows[i]['area'],
      "branch": (rows[i]['branch'] === undefined ? "" : rows[i]['branch']),
      "status": rows[i]['status'],
      "time_instock": (rows[i]['time_instock'] === undefined ? "" : rows[i]['time_instock']),
      "time_takeout": (rows[i]['time_takeout'] === undefined ? "" : rows[i]['time_takeout']),
      "time_installed": (rows[i]['time_installed'] === undefined ? "" : rows[i]['time_installed']),
      "time_maintenance": (rows[i]['time_maintenance'] === undefined ? "" : rows[i]['time_maintenance']),
      'parcel_no': rows[i]['parcel_no']
    })
  }
  res.json(temp);
});

router.post('/find/id', async function(req, res, next){
  var serial = ''
  if (typeof req.body.serial !== undefined){
    serial = req.body.serial
  }

  const doc = new GoogleSpreadsheet(sheet_id);
  await doc.useServiceAccountAuth({
    client_email: 'sheets@api-nodejs-sheet.iam.gserviceaccount.com',
    private_key: '-----BEGIN PRIVATE KEY-----\nMIIEuwIBADANBgkqhkiG9w0BAQEFAASCBKUwggShAgEAAoIBAQCpiQgMnBw6Rha+\n0mrYl7WMXTcszzhAo15FuynnVtZm+58Exlxs+UoixvC4FMf4XjnfMQ/K1k9wGVYd\nBjtceTao7kf3MQQYZgQMesOIcL9g0lCd+NQhFR7cQ8yYo0GsiH4+NOly9uqVUBZS\nZaRaiB0zqnxX9PUd/g1K5L9NMdPfOsbRL/hTtvctcTUWzpUe0xKV3YKlo88DKK7n\n7vi3V1vxrulWZC2Q5PsT2fz2F9c/JM1Zv4yii8LyIMVD/scYxqcQSJCMhH46RJR4\nw2AdAvHR8bB7ZolyI9anLoUYpkx8SshfpMbG8IGq28vyKJGTxk3V38c3SFgT9EsH\nTj+aiaHNAgMBAAECgf9RaGliyUp7jHWEK/PZVrbpC0eFmETGMtD/qvJh7PV068w5\nR+JjA+QVhd+jqcSlbJmmpUPdxy+nJINsI1sUsR/pkMsiZ8J9Yn9GERrTe+KzG28s\n/mFwpeqHI3UI+xLE/Vgzz+tuNrUlIj2VwRRhiR9YC06qElGhlUpfB50d9E9IzvEx\nXPx/M2KBt1H2/RyGMkIxwvaraCZXEIMp+4Hd9NcSCHF6QN/DrsBn8wbODqlHBrwW\nXzjP8xQe1kY9kffDZfct3K1JpDF8BSfYdYFb2PoQYpDaZooc7nw74gTjyPuEY76e\nFiNioRfD8GuqWRpFq59pmzX1t4z2bID90J1dLvMCgYEA3dCzCpMhrGA+pidEFtAz\noKKzLqvqDZhLONPvZR4MhSAk5MMx7Oca+BgsNiV0domP8CwyCOz66UDq8d3uhgPp\nrUFhEJUY38MJsGtGATC/xwrp/D/fjtlYw9qr+7BuQeI2cWzn2vwAvDsdr1dS8DbG\n03eWWmeoITGe62GN1gvBkR8CgYEAw6m5sTReNA6WIrb3qpuhPHp8jkfKyriDU/wP\nbwWokSWy5VC9ytDK3vMUQeW8JBDSRc94C5TH93IEqNfiTXokyspayLD+U6342et8\nC45CxAavhZ+03nRo14yGLMM3RCJNzq15c10MOmzZinZtGGk40Tb8abLFFTChKFb+\nXnmiE5MCgYEAt5ANitPFyxv+3IukKmj0QJxEQnxcfhGpNocVFw4t+EFoprPubgm2\n5NdERy7p2Wrv5ib85SwQXNWvWKd7zAYS7KEtTYbFhe+m5gtwbG3GV1bt2iWVwWaW\nscYWC3yvUfB9c5JXsX5YsW0OnNtcNJRwJ/JQR9J+bbHdvRcHRLqH3qsCgYAGdrPo\ns7cs9xJkoTC7VRrVeaJlvN9pJQFJo/kKjOBNszYetzUd3xnklZkkuCcn9dpMbhck\nIs0cUoru3Prf3loazOaE3Lv6HbQr1sipRRDVkvb3bhg2hRf1jvHbxrMrsApr+nUZ\np4hA4dmp5lGiTc+PscZS45H5KjsYrw8GJfSBuwKBgCtHUM4fKHVhF35RLJ1D78Q2\nbufhZFOY6knsRe2gRVf041Bg8LyO0o3lbG5oowTgLZkD+WWbRPbC7NfJFmxpzgHB\ni+0Pke+dBtCH12zPRnBy6Nrjy6KQVwfAX/KC3qeXvDXJ8Y13m/Ro2yTljqWwRz1O\n4jBsCej2adMQmzWKv8ks\n-----END PRIVATE KEY-----\n',
  });
  await doc.useServiceAccountAuth(require('./client_secret.json'), 'sheets@api-nodejs-sheet.iam.gserviceaccount.com');
  await doc.loadInfo(); 
  const sheet = doc.sheetsByIndex[1]; // the first sheet
  const rows = await sheet.getRows();
  var temp = []
  for (var i = 0;i < rows.length;i++){
    if (serial === rows[i]['serial']){
      temp.push({
        "serial": rows[i]['serial'],
        "area": rows[i]['area'],
        "branch": (rows[i]['branch'] === undefined ? "" : rows[i]['branch']),
        "status": rows[i]['status'],
        "time_instock": (rows[i]['time_instock'] === undefined ? "" : rows[i]['time_instock']),
        "time_takeout": (rows[i]['time_takeout'] === undefined ? "" : rows[i]['time_takeout']),
        "time_installed": (rows[i]['time_installed'] === undefined ? "" : rows[i]['time_installed']),
        "time_maintenance": (rows[i]['time_maintenance'] === undefined ? "" : rows[i]['time_maintenance']),
        'note': (rows[i]['note'] === undefined ? "" : rows[i]['note']),
        'parcel_no': rows[i]['parcel_no']
      })
    }
  }
  res.json(temp);
});

router.post('/update/id', async function(req, res, next){
  
  var serial = ''
  var area = ''
  var branch = ''
  var status = ''
  var time_instock = ''
  var time_takeout = ''
  var time_installed = ''
  var time_maintenance = ""
  var note = ""
  var parcel_no = ""
  
  if (typeof req.body.parcel_no !== undefined){
    parcel_no = req.body.parcel_no
  }
  if (typeof req.body.note !== undefined){
    note = req.body.note
  }
  if (typeof req.body.serial !== undefined){
    serial = req.body.serial
  }
  if (typeof req.body.area !== undefined){
    area = req.body.area
  }
  if (typeof req.body.branch !== undefined){
    branch = req.body.branch
  }
  if (typeof req.body.status !== undefined){
    status = req.body.status
  }
  if (typeof req.body.time_instock !== undefined){
    time_instock = req.body.time_instock
  }
  if (typeof req.body.time_takeout !== undefined){
    time_takeout = req.body.time_takeout
  }
  if (typeof req.body.time_installed !== undefined){
    time_installed = req.body.time_installed
  }
  if (typeof req.body.time_installed !== undefined){
    time_maintenance = req.body.time_maintenance
  }
  try{
    const doc = new GoogleSpreadsheet(sheet_id);
    await doc.useServiceAccountAuth({
      client_email: 'sheets@api-nodejs-sheet.iam.gserviceaccount.com',
      private_key: '-----BEGIN PRIVATE KEY-----\nMIIEuwIBADANBgkqhkiG9w0BAQEFAASCBKUwggShAgEAAoIBAQCpiQgMnBw6Rha+\n0mrYl7WMXTcszzhAo15FuynnVtZm+58Exlxs+UoixvC4FMf4XjnfMQ/K1k9wGVYd\nBjtceTao7kf3MQQYZgQMesOIcL9g0lCd+NQhFR7cQ8yYo0GsiH4+NOly9uqVUBZS\nZaRaiB0zqnxX9PUd/g1K5L9NMdPfOsbRL/hTtvctcTUWzpUe0xKV3YKlo88DKK7n\n7vi3V1vxrulWZC2Q5PsT2fz2F9c/JM1Zv4yii8LyIMVD/scYxqcQSJCMhH46RJR4\nw2AdAvHR8bB7ZolyI9anLoUYpkx8SshfpMbG8IGq28vyKJGTxk3V38c3SFgT9EsH\nTj+aiaHNAgMBAAECgf9RaGliyUp7jHWEK/PZVrbpC0eFmETGMtD/qvJh7PV068w5\nR+JjA+QVhd+jqcSlbJmmpUPdxy+nJINsI1sUsR/pkMsiZ8J9Yn9GERrTe+KzG28s\n/mFwpeqHI3UI+xLE/Vgzz+tuNrUlIj2VwRRhiR9YC06qElGhlUpfB50d9E9IzvEx\nXPx/M2KBt1H2/RyGMkIxwvaraCZXEIMp+4Hd9NcSCHF6QN/DrsBn8wbODqlHBrwW\nXzjP8xQe1kY9kffDZfct3K1JpDF8BSfYdYFb2PoQYpDaZooc7nw74gTjyPuEY76e\nFiNioRfD8GuqWRpFq59pmzX1t4z2bID90J1dLvMCgYEA3dCzCpMhrGA+pidEFtAz\noKKzLqvqDZhLONPvZR4MhSAk5MMx7Oca+BgsNiV0domP8CwyCOz66UDq8d3uhgPp\nrUFhEJUY38MJsGtGATC/xwrp/D/fjtlYw9qr+7BuQeI2cWzn2vwAvDsdr1dS8DbG\n03eWWmeoITGe62GN1gvBkR8CgYEAw6m5sTReNA6WIrb3qpuhPHp8jkfKyriDU/wP\nbwWokSWy5VC9ytDK3vMUQeW8JBDSRc94C5TH93IEqNfiTXokyspayLD+U6342et8\nC45CxAavhZ+03nRo14yGLMM3RCJNzq15c10MOmzZinZtGGk40Tb8abLFFTChKFb+\nXnmiE5MCgYEAt5ANitPFyxv+3IukKmj0QJxEQnxcfhGpNocVFw4t+EFoprPubgm2\n5NdERy7p2Wrv5ib85SwQXNWvWKd7zAYS7KEtTYbFhe+m5gtwbG3GV1bt2iWVwWaW\nscYWC3yvUfB9c5JXsX5YsW0OnNtcNJRwJ/JQR9J+bbHdvRcHRLqH3qsCgYAGdrPo\ns7cs9xJkoTC7VRrVeaJlvN9pJQFJo/kKjOBNszYetzUd3xnklZkkuCcn9dpMbhck\nIs0cUoru3Prf3loazOaE3Lv6HbQr1sipRRDVkvb3bhg2hRf1jvHbxrMrsApr+nUZ\np4hA4dmp5lGiTc+PscZS45H5KjsYrw8GJfSBuwKBgCtHUM4fKHVhF35RLJ1D78Q2\nbufhZFOY6knsRe2gRVf041Bg8LyO0o3lbG5oowTgLZkD+WWbRPbC7NfJFmxpzgHB\ni+0Pke+dBtCH12zPRnBy6Nrjy6KQVwfAX/KC3qeXvDXJ8Y13m/Ro2yTljqWwRz1O\n4jBsCej2adMQmzWKv8ks\n-----END PRIVATE KEY-----\n',
    });
    await doc.useServiceAccountAuth(require('./client_secret.json'), 'sheets@api-nodejs-sheet.iam.gserviceaccount.com');
    await doc.loadInfo(); 
    const sheet = doc.sheetsByIndex[1]; // the first sheet
    const rows = await sheet.getRows();
    var temp = []
    for (var i = 0;i < rows.length;i++){
      if (serial === rows[i]['serial']){

        if (status === 'Instock'){
          rows[i]['serial'] = serial
          rows[i]['area'] = area
          rows[i]['branch'] = ((area === 'กปภ.สำนักงานใหญ่') ? "": branch)
          rows[i]['status'] = status
          rows[i]['time_instock'] = time_instock
          rows[i]['note'] = note
          rows[i]['parcel_no'] = parcel_no
        }else if (status === 'Takeout') {
          rows[i]['serial'] = serial
          rows[i]['area'] = area
          rows[i]['branch'] = ((area === 'กปภ.สำนักงานใหญ่') ? "": branch)
          rows[i]['status'] = status
          rows[i]['time_takeout'] = time_takeout
          rows[i]['note'] = note
          rows[i]['parcel_no'] = parcel_no
        }else if (status === 'Installed') {
          rows[i]['serial'] = serial
          rows[i]['area'] = area
          rows[i]['branch'] = ((area === 'กปภ.สำนักงานใหญ่') ? "": branch)
          rows[i]['status'] = status
          rows[i]['time_installed'] = time_installed
          rows[i]['note'] = note
          rows[i]['parcel_no'] = parcel_no
        }else if (status === 'Maintenance'){
          rows[i]['serial'] = serial
          rows[i]['area'] = area
          rows[i]['branch'] = branch
          rows[i]['status'] = status
          rows[i]['time_installed'] = ''
          rows[i]['time_maintenance'] = time_maintenance
          rows[i]['note'] = note
          rows[i]['parcel_no'] = parcel_no
        }
        // rows[i]['serial'] = serial
        // rows[i]['area'] = area
        // rows[i]['branch'] = branch
        // rows[i]['status'] = status
        // rows[i]['time_instock'] = time_instock
        // rows[i]['time_takeout'] = time_takeout
        // rows[i]['time_installed'] = time_installed
        await rows[i].save(); // save updates
      }
    }
    // insert log
    const sheet_ = doc.sheetsByIndex[2];
    var time = ''
    if (status === 'Instock'){
      time = time_instock
    }else if (status === 'Takeout') {
      time = time_takeout
    }else if (status === 'Installed') {
      time = time_installed
    }else if (status === 'Maintenance'){
      time = time_maintenance
    }
    var temp = [{ 
      serial: serial, 
      area: area,
      branch: branch,
      status: status,
      datetime: time,
      note: note,
    }]
    const moreRows_ = await sheet_.addRows(temp);
    res.json({
      status: true
    });
  }catch(e){
    res.json({error: e,status: false});
  }
});

router.put('/add', async function(req, res, next){
  
  var serial = ''
  var area = ''
  var branch = ''
  var status = ''
  var time_instock = ''
  var time_takeout = ''
  var time_installed = ''
  var note = ""
  var parcel_no = ""

  if (typeof req.body.parcel_no !== undefined){
    parcel_no = req.body.parcel_no
  }
  if (typeof req.body.note !== undefined){
    note = req.body.note
  }
  if (typeof req.body.serial !== undefined){
    serial = req.body.serial
  }
  if (typeof req.body.area !== undefined){
    area = req.body.area
  }
  if (typeof req.body.branch !== undefined){
    branch = req.body.branch
  }
  if (typeof req.body.status !== undefined){
    status = req.body.status
  }
  if (typeof req.body.time_instock !== undefined){
    time_instock = req.body.time_instock
  }
  if (typeof req.body.time_takeout !== undefined){
    time_takeout = req.body.time_takeout
  }
  if (typeof req.body.time_installed !== undefined){
    time_installed = req.body.time_installed
  }
  try{
    const doc = new GoogleSpreadsheet(sheet_id);
    await doc.useServiceAccountAuth({
      client_email: 'sheets@api-nodejs-sheet.iam.gserviceaccount.com',
      private_key: '-----BEGIN PRIVATE KEY-----\nMIIEuwIBADANBgkqhkiG9w0BAQEFAASCBKUwggShAgEAAoIBAQCpiQgMnBw6Rha+\n0mrYl7WMXTcszzhAo15FuynnVtZm+58Exlxs+UoixvC4FMf4XjnfMQ/K1k9wGVYd\nBjtceTao7kf3MQQYZgQMesOIcL9g0lCd+NQhFR7cQ8yYo0GsiH4+NOly9uqVUBZS\nZaRaiB0zqnxX9PUd/g1K5L9NMdPfOsbRL/hTtvctcTUWzpUe0xKV3YKlo88DKK7n\n7vi3V1vxrulWZC2Q5PsT2fz2F9c/JM1Zv4yii8LyIMVD/scYxqcQSJCMhH46RJR4\nw2AdAvHR8bB7ZolyI9anLoUYpkx8SshfpMbG8IGq28vyKJGTxk3V38c3SFgT9EsH\nTj+aiaHNAgMBAAECgf9RaGliyUp7jHWEK/PZVrbpC0eFmETGMtD/qvJh7PV068w5\nR+JjA+QVhd+jqcSlbJmmpUPdxy+nJINsI1sUsR/pkMsiZ8J9Yn9GERrTe+KzG28s\n/mFwpeqHI3UI+xLE/Vgzz+tuNrUlIj2VwRRhiR9YC06qElGhlUpfB50d9E9IzvEx\nXPx/M2KBt1H2/RyGMkIxwvaraCZXEIMp+4Hd9NcSCHF6QN/DrsBn8wbODqlHBrwW\nXzjP8xQe1kY9kffDZfct3K1JpDF8BSfYdYFb2PoQYpDaZooc7nw74gTjyPuEY76e\nFiNioRfD8GuqWRpFq59pmzX1t4z2bID90J1dLvMCgYEA3dCzCpMhrGA+pidEFtAz\noKKzLqvqDZhLONPvZR4MhSAk5MMx7Oca+BgsNiV0domP8CwyCOz66UDq8d3uhgPp\nrUFhEJUY38MJsGtGATC/xwrp/D/fjtlYw9qr+7BuQeI2cWzn2vwAvDsdr1dS8DbG\n03eWWmeoITGe62GN1gvBkR8CgYEAw6m5sTReNA6WIrb3qpuhPHp8jkfKyriDU/wP\nbwWokSWy5VC9ytDK3vMUQeW8JBDSRc94C5TH93IEqNfiTXokyspayLD+U6342et8\nC45CxAavhZ+03nRo14yGLMM3RCJNzq15c10MOmzZinZtGGk40Tb8abLFFTChKFb+\nXnmiE5MCgYEAt5ANitPFyxv+3IukKmj0QJxEQnxcfhGpNocVFw4t+EFoprPubgm2\n5NdERy7p2Wrv5ib85SwQXNWvWKd7zAYS7KEtTYbFhe+m5gtwbG3GV1bt2iWVwWaW\nscYWC3yvUfB9c5JXsX5YsW0OnNtcNJRwJ/JQR9J+bbHdvRcHRLqH3qsCgYAGdrPo\ns7cs9xJkoTC7VRrVeaJlvN9pJQFJo/kKjOBNszYetzUd3xnklZkkuCcn9dpMbhck\nIs0cUoru3Prf3loazOaE3Lv6HbQr1sipRRDVkvb3bhg2hRf1jvHbxrMrsApr+nUZ\np4hA4dmp5lGiTc+PscZS45H5KjsYrw8GJfSBuwKBgCtHUM4fKHVhF35RLJ1D78Q2\nbufhZFOY6knsRe2gRVf041Bg8LyO0o3lbG5oowTgLZkD+WWbRPbC7NfJFmxpzgHB\ni+0Pke+dBtCH12zPRnBy6Nrjy6KQVwfAX/KC3qeXvDXJ8Y13m/Ro2yTljqWwRz1O\n4jBsCej2adMQmzWKv8ks\n-----END PRIVATE KEY-----\n',
    });
    await doc.useServiceAccountAuth(require('./client_secret.json'), 'sheets@api-nodejs-sheet.iam.gserviceaccount.com');
    await doc.loadInfo(); 
    const sheet = doc.sheetsByIndex[1];
    var temp = []
    temp = [{ 
      serial: serial, 
      area: area,
      branch: branch,
      status: status,
      time_instock: time_instock,
      time_takeout: time_takeout,
      time_installed: time_installed,
      note: note,
      parcel_no: parcel_no
    }]
    const moreRows = await sheet.addRows(temp);

    // insert log
    const sheet_ = doc.sheetsByIndex[2];
    var time = ''
    if (status === 'Instock'){
      time = time_instock
    }else if (status === 'Takeout') {
      time = time_takeout
    }else if (status === 'Installed') {
      time = time_installed
    }
    var temp = [{ 
      serial: serial, 
      area: area,
      branch: branch,
      status: status,
      datetime: time,
      note, note
    }]
    const moreRows_ = await sheet_.addRows(temp);
    res.json({
      status: true
    });
  }catch(e){
    res.json({
      error: e,
      status: false
    });
  }
});

router.post('/project/list', async function(req, res, next){
  const doc = new GoogleSpreadsheet(sheet_id);
  await doc.useServiceAccountAuth({
    client_email: 'sheets@api-nodejs-sheet.iam.gserviceaccount.com',
    private_key: '-----BEGIN PRIVATE KEY-----\nMIIEuwIBADANBgkqhkiG9w0BAQEFAASCBKUwggShAgEAAoIBAQCpiQgMnBw6Rha+\n0mrYl7WMXTcszzhAo15FuynnVtZm+58Exlxs+UoixvC4FMf4XjnfMQ/K1k9wGVYd\nBjtceTao7kf3MQQYZgQMesOIcL9g0lCd+NQhFR7cQ8yYo0GsiH4+NOly9uqVUBZS\nZaRaiB0zqnxX9PUd/g1K5L9NMdPfOsbRL/hTtvctcTUWzpUe0xKV3YKlo88DKK7n\n7vi3V1vxrulWZC2Q5PsT2fz2F9c/JM1Zv4yii8LyIMVD/scYxqcQSJCMhH46RJR4\nw2AdAvHR8bB7ZolyI9anLoUYpkx8SshfpMbG8IGq28vyKJGTxk3V38c3SFgT9EsH\nTj+aiaHNAgMBAAECgf9RaGliyUp7jHWEK/PZVrbpC0eFmETGMtD/qvJh7PV068w5\nR+JjA+QVhd+jqcSlbJmmpUPdxy+nJINsI1sUsR/pkMsiZ8J9Yn9GERrTe+KzG28s\n/mFwpeqHI3UI+xLE/Vgzz+tuNrUlIj2VwRRhiR9YC06qElGhlUpfB50d9E9IzvEx\nXPx/M2KBt1H2/RyGMkIxwvaraCZXEIMp+4Hd9NcSCHF6QN/DrsBn8wbODqlHBrwW\nXzjP8xQe1kY9kffDZfct3K1JpDF8BSfYdYFb2PoQYpDaZooc7nw74gTjyPuEY76e\nFiNioRfD8GuqWRpFq59pmzX1t4z2bID90J1dLvMCgYEA3dCzCpMhrGA+pidEFtAz\noKKzLqvqDZhLONPvZR4MhSAk5MMx7Oca+BgsNiV0domP8CwyCOz66UDq8d3uhgPp\nrUFhEJUY38MJsGtGATC/xwrp/D/fjtlYw9qr+7BuQeI2cWzn2vwAvDsdr1dS8DbG\n03eWWmeoITGe62GN1gvBkR8CgYEAw6m5sTReNA6WIrb3qpuhPHp8jkfKyriDU/wP\nbwWokSWy5VC9ytDK3vMUQeW8JBDSRc94C5TH93IEqNfiTXokyspayLD+U6342et8\nC45CxAavhZ+03nRo14yGLMM3RCJNzq15c10MOmzZinZtGGk40Tb8abLFFTChKFb+\nXnmiE5MCgYEAt5ANitPFyxv+3IukKmj0QJxEQnxcfhGpNocVFw4t+EFoprPubgm2\n5NdERy7p2Wrv5ib85SwQXNWvWKd7zAYS7KEtTYbFhe+m5gtwbG3GV1bt2iWVwWaW\nscYWC3yvUfB9c5JXsX5YsW0OnNtcNJRwJ/JQR9J+bbHdvRcHRLqH3qsCgYAGdrPo\ns7cs9xJkoTC7VRrVeaJlvN9pJQFJo/kKjOBNszYetzUd3xnklZkkuCcn9dpMbhck\nIs0cUoru3Prf3loazOaE3Lv6HbQr1sipRRDVkvb3bhg2hRf1jvHbxrMrsApr+nUZ\np4hA4dmp5lGiTc+PscZS45H5KjsYrw8GJfSBuwKBgCtHUM4fKHVhF35RLJ1D78Q2\nbufhZFOY6knsRe2gRVf041Bg8LyO0o3lbG5oowTgLZkD+WWbRPbC7NfJFmxpzgHB\ni+0Pke+dBtCH12zPRnBy6Nrjy6KQVwfAX/KC3qeXvDXJ8Y13m/Ro2yTljqWwRz1O\n4jBsCej2adMQmzWKv8ks\n-----END PRIVATE KEY-----\n',
  });
  await doc.useServiceAccountAuth(require('./client_secret.json'), 'sheets@api-nodejs-sheet.iam.gserviceaccount.com');
  await doc.loadInfo(); 
  const sheet = doc.sheetsByIndex[3]; // the first sheet
  const rows = await sheet.getRows();
  var temp = []
  for (var i = 0;i < rows.length;i++){
    temp.push({
      "code": rows[i]['code'],
      "project_name": rows[i]['project_name'],
    })
  }
  res.json(temp);
})

router.post('/upload/csv', async function(req, res, next){
  var csv = []
  if (typeof req.body.data !== undefined){
    csv = req.body.data
  }
  try{
    const doc = new GoogleSpreadsheet(sheet_id);
    await doc.useServiceAccountAuth({
      client_email: 'sheets@api-nodejs-sheet.iam.gserviceaccount.com',
      private_key: '-----BEGIN PRIVATE KEY-----\nMIIEuwIBADANBgkqhkiG9w0BAQEFAASCBKUwggShAgEAAoIBAQCpiQgMnBw6Rha+\n0mrYl7WMXTcszzhAo15FuynnVtZm+58Exlxs+UoixvC4FMf4XjnfMQ/K1k9wGVYd\nBjtceTao7kf3MQQYZgQMesOIcL9g0lCd+NQhFR7cQ8yYo0GsiH4+NOly9uqVUBZS\nZaRaiB0zqnxX9PUd/g1K5L9NMdPfOsbRL/hTtvctcTUWzpUe0xKV3YKlo88DKK7n\n7vi3V1vxrulWZC2Q5PsT2fz2F9c/JM1Zv4yii8LyIMVD/scYxqcQSJCMhH46RJR4\nw2AdAvHR8bB7ZolyI9anLoUYpkx8SshfpMbG8IGq28vyKJGTxk3V38c3SFgT9EsH\nTj+aiaHNAgMBAAECgf9RaGliyUp7jHWEK/PZVrbpC0eFmETGMtD/qvJh7PV068w5\nR+JjA+QVhd+jqcSlbJmmpUPdxy+nJINsI1sUsR/pkMsiZ8J9Yn9GERrTe+KzG28s\n/mFwpeqHI3UI+xLE/Vgzz+tuNrUlIj2VwRRhiR9YC06qElGhlUpfB50d9E9IzvEx\nXPx/M2KBt1H2/RyGMkIxwvaraCZXEIMp+4Hd9NcSCHF6QN/DrsBn8wbODqlHBrwW\nXzjP8xQe1kY9kffDZfct3K1JpDF8BSfYdYFb2PoQYpDaZooc7nw74gTjyPuEY76e\nFiNioRfD8GuqWRpFq59pmzX1t4z2bID90J1dLvMCgYEA3dCzCpMhrGA+pidEFtAz\noKKzLqvqDZhLONPvZR4MhSAk5MMx7Oca+BgsNiV0domP8CwyCOz66UDq8d3uhgPp\nrUFhEJUY38MJsGtGATC/xwrp/D/fjtlYw9qr+7BuQeI2cWzn2vwAvDsdr1dS8DbG\n03eWWmeoITGe62GN1gvBkR8CgYEAw6m5sTReNA6WIrb3qpuhPHp8jkfKyriDU/wP\nbwWokSWy5VC9ytDK3vMUQeW8JBDSRc94C5TH93IEqNfiTXokyspayLD+U6342et8\nC45CxAavhZ+03nRo14yGLMM3RCJNzq15c10MOmzZinZtGGk40Tb8abLFFTChKFb+\nXnmiE5MCgYEAt5ANitPFyxv+3IukKmj0QJxEQnxcfhGpNocVFw4t+EFoprPubgm2\n5NdERy7p2Wrv5ib85SwQXNWvWKd7zAYS7KEtTYbFhe+m5gtwbG3GV1bt2iWVwWaW\nscYWC3yvUfB9c5JXsX5YsW0OnNtcNJRwJ/JQR9J+bbHdvRcHRLqH3qsCgYAGdrPo\ns7cs9xJkoTC7VRrVeaJlvN9pJQFJo/kKjOBNszYetzUd3xnklZkkuCcn9dpMbhck\nIs0cUoru3Prf3loazOaE3Lv6HbQr1sipRRDVkvb3bhg2hRf1jvHbxrMrsApr+nUZ\np4hA4dmp5lGiTc+PscZS45H5KjsYrw8GJfSBuwKBgCtHUM4fKHVhF35RLJ1D78Q2\nbufhZFOY6knsRe2gRVf041Bg8LyO0o3lbG5oowTgLZkD+WWbRPbC7NfJFmxpzgHB\ni+0Pke+dBtCH12zPRnBy6Nrjy6KQVwfAX/KC3qeXvDXJ8Y13m/Ro2yTljqWwRz1O\n4jBsCej2adMQmzWKv8ks\n-----END PRIVATE KEY-----\n',
    });
    await doc.useServiceAccountAuth(require('./client_secret.json'), 'sheets@api-nodejs-sheet.iam.gserviceaccount.com');
    await doc.loadInfo(); 
    const sheet = doc.sheetsByIndex[3]; // the first sheet
    const rows = await sheet.getRows();
    /**
     * check data in sheet
     */
    var havenotdata = []
    for (var i = 0;i < rows.length;i++){
      for (var m = 0;m < csv.length;m++){
        if (csv[m].serial === rows[i].serial){
          /**
           * have data
           */
          rows[i]['code'] = csv[m].code
          rows[i]['project_name'] = csv[m].project_names
          await rows[i].save();
          csv.splice(m, 1);
        }
      }
    }
    const moreRows = await sheet.addRows(csv);
    res.json({
      status: true
    });
  }catch(e){
    res.json({
      error: e,
      status: false
    });
  }
});

module.exports = router;
