var mongoose = require('mongoose');

var SchemaTypes = mongoose.Schema.Types;

var LogStatusSchema = new mongoose.Schema({
    station_id: String, 
    type: String,
    stop_time: Date,
    start_time: Date,
    period_ms: String,
});

mongoose.model('LogStatuses', LogStatusSchema);