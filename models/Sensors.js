var mongoose = require('mongoose');

var SchemaTypes = mongoose.Schema.Types;

var SensorSchema = new mongoose.Schema({
    station_id:String, 
    PT:String,
    TT:String,
    Positions:Array,
    Loads:Array,
    timestamp_s:Date,
    timestamp_g:Date,
    speed:String,
    Parameter: Object,
    PumpCardLoads: Array,
    PumpCardPositions: Array,
    pump_status: Boolean,
    rtu_status:Boolean,
});

mongoose.model('Sensor', SensorSchema);