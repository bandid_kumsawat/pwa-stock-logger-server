var mongoose = require('mongoose');

var SchemaTypes = mongoose.Schema.Types;

var SensorSchema = new mongoose.Schema({
  station_id:String,
  Loads:Array,
  Positions:Array,
  PT:String,
  TT:String,
  DateTime:Date,
  Speed: String,
  PumpCardLoads: Array,
  PumpCardPositions: Array,
});

mongoose.model('Raw', SensorSchema);