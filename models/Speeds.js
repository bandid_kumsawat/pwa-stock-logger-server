var mongoose = require('mongoose');

var SchemaTypes = mongoose.Schema.Types;

var SpeedSchema = new mongoose.Schema({
    station_id:String, 
    timestamp_s:Date,
    timestamp_g:Date,
    speed: String,
});

mongoose.model('Speed', SpeedSchema);